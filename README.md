---
title: "Prueba técnica"
author: "Adrián Gutiérrez"
date: "`r Sys.Date()`"
output: pdf_document
---

## Acercamiento
Decidí concentrarme en áreas de alta marginación, puesto que en estas zonas el acceso a servicios públicos como educación o salud suele ser escaso o incluso inexistente. Para conocer las áreas con alto nivel de marginación utilicé los datos del índice de marginación por colonia de la CONAPO para el año 2020. Realicé una prueba de autocorrelación local de Moran con el índice de marginación (figura A). Utilice 1000 simulaciones de Monte-Carlo y obtuve un valor de p  < 0.001, lo cual permite rechazar la hipótesis nula de que no existe autocorrelación espacial. En la figura B se observa que la zona sureste de la ZMVM (Chalco; rectángulo) se encuentran agrupados polígonos con valores bajos del índice de marginación.
```{r echo=FALSE, message=FALSE, warning=FALSE}
library(sfnetworks)
library(stplanr)
library(sf)
library(tidyverse)
library(sfdep)
library(tmap) 
library(patchwork)
library(basemaps)

ttm()
tmap_options(check.and.fix = TRUE) 

# Delimitación de la ZMVM
metropolitanas <- read_csv('Base_delimitacionZM_00-10.csv') 

#unique(metropolitanas$ZM) 

metropolitanas <- metropolitanas[metropolitanas$ZM == 'Zona metropolitana del Valle de México',]
metropolitanas <- filter(metropolitanas, 
                         AÑO == 2010) %>% 
  select(ZM, CVE_MUN)

marginacion <- read_sf('marginacion20/colonias_imc2020.shp') 

marginacion <- inner_join(marginacion, 
                          metropolitanas,
                          by = c('CVE_MUN')) %>% 
  filter(ZM == 'Zona metropolitana del Valle de México' & 
           NOM_ENT == 'México') 

rm(metropolitanas)

# Cálculo de I de Moran para la autocorrelación espacial

marginacion <- marginacion|> 
  mutate(nb = st_contiguity(geometry),
         wt = st_weights(nb,
                         allow_zero = T))

#marginacion |> 
#  summarise(i = list(global_moran_perm(IMN_2020, 
#                                       nb,
#                                       wt,
#                                       nsim = 1000,
#                                       zero.policy = T,
#                                       na.action = na.omit))) |> 
#  pull(i)

p_lag <- marginacion |> 
  mutate(imn_lag = st_lag(IMN_2020,
                          nb, 
                          wt,
                          allow_zero = T)) |>
  filter(imn_lag > 0) %>% 
  ggplot(aes(IMN_2020, imn_lag)) +
  geom_point() +
  geom_vline(aes(xintercept = mean(IMN_2020)), lty = 2) +
  geom_hline(aes(yintercept = mean(IMN_2020)), lty = 2) +
  geom_smooth(method = "lm") +
  theme_minimal()

marginacion <- marginacion %>% 
  mutate(lisa = local_moran(IMN_2020,
                            nb,
                            wt,
                            zero.policy = T)) %>% 
  unnest(lisa)

area_estudio <- st_bbox(c(xmin = 2804966, 
                          xmax = 2856741,
                          ymin = 774398.1,
                          ymax = 831658.8),
                        crs = st_crs(marginacion)) %>% 
  st_as_sfc()
p_marginacion <- ggplot() +
  geom_sf(data = marginacion,
          aes(fill = mean),
          lwd = 0.1,
          inherit.aes = F) +
  geom_sf(data = area_estudio,
          fill = NA) +
  ggthemes::theme_map() +
  labs(fill = NULL)

p_lag + p_marginacion + plot_annotation(tag_levels = 'A')

# vialidades <- st_read('red_nal_caminos.gpkg',
#                       'red_vial') %>% 
#   st_transform(st_crs(marginacion)) %>% 
#   st_crop(marginacion)

#st_write(vialidades,
#         'itdp.gpkg',
#         'vialidades',
#         delete_layer = T)


```

```{r echo=FALSE, message=FALSE, warning=FALSE}
puntos <- tribble(~nombre, ~geom,
                  'la paz', st_point(c(-98.96093455739799,
                                       19.35057001404076)),
                  'chalco', st_point(c(-98.893896,
                                       19.259465))) %>% 
  st_as_sf(crs = 4326) %>% 
  st_transform(st_crs(marginacion)) 


caminos <- st_read('itdp.gpkg',
                   'vialidades',
                   quiet = T) %>% 
  st_crop(area_estudio) %>% 
  st_cast('LINESTRING') %>% 
  filter(TIPO_VIAL != "Carretera" & 
           !str_detect(NOMBRE, 
                       regex('lateral',
                             ignore_case = T)))

net = as_sfnetwork(caminos, directed = F) %>% 
  activate("edges") %>%
  mutate(weight = edge_length())

paths = st_network_paths(net, 
                         from = puntos$geom[1], 
                         to = puntos$geom[c(2)],
                         weights = net$weight)

linea <- net %>% 
  activate('edges') %>% 
  slice(unlist(paths$edge_paths)) %>%
  st_as_sf() %>% 
  summarise() 

#st_write(linea,
#         'itdp.gpkg',
#         'ruta',
#         delete_layer = T)
```

## Diseño de la ruta
A partir del área de estudio determiné opciones para conectar el área con otros medios de transporte para incentivar la movilidad multimodal y facilitar el movimiento de las personas. Utilicé la estación del metro La Paz para conectarla con Chalco. Para ello hice un análisis de redes con la Red Nacional de Caminos. Para propósitos de este trabajo decidí sacar las carreteras y las laterales. Elegí un BRT (longitud de `r units::drop_units(round(units::set_units(sum(st_length(linea)), km), 2))` kilómetros) como para ocupar esta línea. La razón es que la implementación no es excesivamente cara, el número de carriles por el que pasa la ruta es de entre 2 y 4 carriles, lo que permitiría que este tuviese carril confinado en algunos tramos, haciendo, permitiendo que el viaje del autobús sea más rápido que el del transporte privado o el concesionado. [El tiempo de implementación del BRT es de entre un año y un año y medio](https://policy.tti.tamu.edu/strategy/bus-rapid-transit/), lo que lo convierte a una alternativa más rápida que la expansión o la creación de nuevas líneas de metro o tranvía. También el requerimiento de materiales y de energía es menor, por lo que es una opción con un impacto ambiental y a la salud (polvo, ruido, emisiones) menor, de igual manera [la implementación del BRT se asocia con una disminución en contaminantes en la CDMX](https://www.sciencedirect.com/science/article/abs/pii/S0967070X17301592), lo que abona a la mitigación del cambio climático. 

```{r echo=FALSE, message=FALSE, warning=FALSE}
puntos <- tribble(~nombre, ~geom,
                  'la paz', st_point(c(-98.96093455739799,
                                       19.35057001404076)),
                  'chalco', st_point(c(-98.893896,
                                       19.259465))) %>% 
  st_as_sf(crs = 4326) %>% 
  st_transform(st_crs(marginacion)) 


caminos <- st_read('itdp.gpkg',
                   'vialidades',
                   quiet = T) %>% 
  st_crop(area_estudio) %>% 
  st_cast('LINESTRING') %>% 
  filter(TIPO_VIAL != "Carretera" & 
           !str_detect(NOMBRE, 
                       regex('lateral',
                             ignore_case = T)))

net = as_sfnetwork(caminos, directed = F) %>% 
  activate("edges") %>%
  mutate(weight = edge_length())

paths = st_network_paths(net, 
                         from = puntos$geom[1], 
                         to = puntos$geom[c(2)],
                         weights = net$weight)

linea <- net %>% 
  activate('edges') %>% 
  slice(unlist(paths$edge_paths)) %>%
  st_as_sf() %>% 
  summarise() 

#st_write(linea,
#         'itdp.gpkg',
#         'ruta',
#         delete_layer = T)




ggplot() +
  geom_sf(data = marginacion,
          aes(fill = mean),
          lwd = 0.1,
          inherit.aes = F) +
  geom_sf(data = caminos,
          lwd = 0.1,
          color = 'grey86') +
  geom_sf(data = linea,
          lwd = 2,
          aes(color = 'Línea propuesta')) +
  geom_sf(data = puntos,
          aes(shape = nombre),
          size = 2,
          color = c("#008B00")) +
  scale_color_manual(values = 'black') +
  labs(fill = NULL,
       shape = NULL,
       color = NULL) +
  coord_sf(xlim = st_bbox(area_estudio)[c(1,3)],
           ylim = st_bbox(area_estudio)[c(2,4)]) +
  ggthemes::theme_map()

```

## Accesibilidad
A partir de la línea que definí como nueva ruta creé un buffer de 400 metros (distancia recomendada como óptima para ser caminable) para conocer los servicios accesibles dentro de la zona de influencia de la línea. Los resultados se muestran en la siguiente tabla.

```{r echo=FALSE, message=FALSE, warning=FALSE}
library(gt)

buffer <- st_buffer(linea, 
                    dist = 400)

# denue <- lapply(list.files('denue/',
#            pattern = '\\.csv',
#            full.names = T),
#        read_csv) %>% 
#   bind_rows() %>% 
#   st_as_sf(coords = c('longitud', 
#                       'latitud'),
#            crs = 4326) %>% 
#   st_transform(., st_crs(buffer)) %>% 
#   st_filter(buffer)
# 
# 
#  
# denue <- denue %>% 
#   filter(str_detect(codigo_act, '^461|^462|^464|^61|^62')) %>% 
#   transmute(tipo = case_when(str_detect(codigo_act, '^461') ~ 'Abarrotes',
#                              str_detect(codigo_act, '^462') ~ 'Autoservicio',
#                              str_detect(codigo_act, '^464') ~ 'Cuidado de la salud',
#                              str_detect(codigo_act, '^61') ~ 'Servicios educativos',
#                              str_detect(codigo_act, '^62') ~ 'Servicios de salud')) %>% 
#   count(tipo) %>% 
#   arrange(desc(n))
# 
# st_write(denue,
#          'itdp.gpkg',
#          'denue',
#          delete_layer = T)

denue <- read_sf('itdp.gpkg',
                 'denue')

denue %>%
  st_drop_geometry() %>% 
  rename(Servicio = 1,
         Total = 2) %>% 
  gt() %>% 
  grand_summary_rows(columns = 'Total',
                     fns = list(Total = ~ sum(.)))



```

## Población beneficiada 
Utilizando el área de influencia de la línea propuesta y los resultados por AGEB del censo del año 2020 se estimó la población total, población femenina, masculina, mujeres de 15 a 49 años y personas de 0 a 11 años beneficiados directamente.

```{r  echo=FALSE, message=FALSE, warning=FALSE}
marcogeo <- read_sf('marcogeoestadistico/conjunto_de_datos/15a.shp') %>% 
  st_transform(st_crs(marginacion)) %>% 
  st_crop(marginacion)

censo <- read_csv('RESAGEBURB_15CSV20.csv',
                  show_col_types = F) %>% 
  unite('CVEGEO',
        ENTIDAD, 
        MUN, 
        LOC, 
        AGEB, 
        sep = '')


censo <- inner_join(marcogeo, censo) %>% 
  st_filter(buffer)
rm(marcogeo)

censo %>% 
  st_drop_geometry() %>% 
  select(POBTOT, 
         POBFEM,
         POBMAS,
         P_15A49_F,
         P_0A2,
         P_3A5,
         P_6A11) %>% 
  mutate(across(c(POBFEM:P_6A11), ~parse_integer(.))) %>% 
  mutate(niños = P_0A2 + P_3A5 + P_6A11,
         .keep = 'unused') %>% 
  summarise(across(everything(), ~ sum(., na.rm = T))) %>% 
  pivot_longer(cols = everything()) %>% 
  mutate(name = c('Población total',
                  'Población femenina',
                  'Población masculina',
                  'Mujeres de 15 a 49 años',
                  'Personas de 0 a 11 años')) %>% 
  gt(rowname_col = 'name') %>% 
  fmt_integer('value') %>% 
  cols_label(value = 'Población') 
  

```
## Discusión
Esta línea permite a las mujeres de Chalco la movilidad necesaria para poder llevar a cabo sus actividades de cuidado al proveerles de un medio de transporte rápido y eficiente que les permite moverse a los servicios que necesitan. Además de eso, permite la conexión con el resto de la Ciudad de México al tener como parada una estación de metro, aumentando aún más su capacidad de movimiento eficiente y accesible. La reducción de contaminantes causados por la combustión, de tiempo de traslado les permite mejorar su calidad de vida y su salud, aumentando su nivel de bienestar general. 

## Análisis futuros
- Me gustaría hacer un análisis más profundo de la accesibilidad a la ruta de transporte, por ejemplo, el uso de los algoritmos de pgRouting para obtener un mejor detalle. 
- De igual manera intenté utilizar la librería r5r para el ruteo multimodal, pero mi sistema operativo (Arch Linux) no me permitió instalar la paquetería necesaria. Me gustaría poder replicar y expandir los datos del visualizador de accesibilidad urbana para más ciudades del país. 
- Hacer una propuesta de las paradas potenciales que la línea podría tener con un análisis de accesibilidad más profundo. 
- Utilizar las API de Google Maps o Here para estimar los tiempos de viaje en las zonas donde un carril confinado no sea viable. Después de obtener suficientes datos del funcionamiento me gustaría crear un modelo con [Prophet](https://facebook.github.io/prophet/) para predecir los tiempo de traslado y analizar la variación horaria/diaria/semanal/mensual y los efectos de los días feriados en el tiempo de traslado y en el número de usuarios. 